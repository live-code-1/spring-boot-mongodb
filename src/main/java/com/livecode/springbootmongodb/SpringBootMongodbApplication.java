package com.livecode.springbootmongodb;

import com.livecode.springbootmongodb.model.Member;
import com.livecode.springbootmongodb.repository.MemberRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@Log4j2
public class SpringBootMongodbApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMongodbApplication.class, args);
    }

    @Autowired
    private MemberRepository memberRepository;

    @Override
    public void run(String... args) throws Exception {

        // Create 3 Members
        Member member1 = Member.builder()
                .firstName("Live")
                .lastName("Code")
                .email("live.code@mail.com")
                .build();

        Member member2 = Member.builder()
                .firstName("YouTube")
                .lastName("Member")
                .email("youtubeMember@mail.com")
                .build();

        Member member3 = Member.builder()
                .firstName("YouTube")
                .lastName("Admin")
                .email("youtuberAdmin@gmail.com")
                .build();

        List<Member> memberList = Arrays.asList(member1,member2,member3);

        // Insert into MongoDB if email not exist
        for(Member member: memberList){
            if(!memberRepository.existsMemberByEmail(member.getEmail())){
                memberRepository.save(member);
                log.info("Record inserted");
            }else{
                log.warn("Email exist! Skip data insertion.");
            }
        }

        // Query MongoDB by firstName
        List<Member> memberListWithYouTubeFirstName = memberRepository.findByFirstName("YouTube");
        List<Member> memberListWithLiveFirstName = memberRepository.findByFirstName("Live");

        log.info("Member List with YouTube First Name");
        memberListWithYouTubeFirstName.forEach(log::info);

        log.info("Member list with Live First Name");
        memberListWithLiveFirstName.forEach(log::info);


        // Update MongoDB
        if(memberListWithLiveFirstName.stream().findFirst().isPresent()){
            Member m = memberListWithLiveFirstName.stream().findFirst().get();
            m.setFirstName("Live Code");
            m.setLastName("Supper");;
            memberRepository.save(m);
            log.info("Updated");
        }

        // Delete all YouTube members
        memberRepository.deleteAll(memberListWithYouTubeFirstName);
        log.info("Deleted all YouTube members");

        // Print all records in database
        memberRepository.findAll().forEach(log::info);
    }
}
