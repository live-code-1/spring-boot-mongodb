package com.livecode.springbootmongodb.repository;

import com.livecode.springbootmongodb.model.Member;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MemberRepository extends MongoRepository<Member,String> {

    List<Member> findByFirstName(String firstName);

    Boolean existsMemberByEmail(String email);
}
